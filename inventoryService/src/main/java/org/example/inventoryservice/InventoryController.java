package org.example.inventoryservice;

import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/inventory")
public class InventoryController {
    private final InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @PostMapping("/incubator/unlock")
    public void unlockIncubator(@RequestHeader int playerId) {
        inventoryService.unlockIncubator(playerId);
    }

    @PostMapping("/incubator/use")
    public boolean useIncubator(@RequestHeader int playerId) {
        return inventoryService.useIncubator(playerId);
    }

    @PostMapping("/egg/remove")
    public boolean removeEggToInventory(@RequestHeader int playerId) {
        return inventoryService.removeEggToInventory(playerId);
    }

    @PostMapping("/add")
    public void addObjectToInventory(@RequestBody String name, @RequestHeader int playerId) {
        inventoryService.addObjectToInventory(name, playerId);
    }

    @PostMapping("/balance/update")
    public boolean updatePlayerBalance(@RequestBody int number, @RequestHeader int playerId) {
        return inventoryService.updatePlayerBalance(number, playerId);
    }

    @PostMapping("/")
    public String getInventory(@RequestHeader int playerId) {
        return inventoryService.getInventory(playerId);
    }
}

