package org.example.inventoryservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class InventoryService {

    private final RedisTemplate<String, String> redisTemplate;

    @Autowired
    public InventoryService(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    private String AddNewPlayerInventory(String playerId) {
        ObjectMapper objectMapper = new ObjectMapper();
        String playerJson = "";

        Map<String, Integer> playerData = new HashMap<>();
        playerData.put("eggQuantity", 0);
        playerData.put("incubatorQuantity", 0);
        playerData.put("incubatorInUse", 0);
        playerData.put("balance", 25);

        try {
            playerJson = objectMapper.writeValueAsString(playerData);
            redisTemplate.opsForValue().set(playerId, playerJson);

        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }
        return playerJson;
    }

    public void unlockIncubator(int playerId) {
        ObjectMapper objectMapper = new ObjectMapper();

        String playerJson = redisTemplate.opsForValue().get(String.valueOf(playerId));

        if (playerJson == null) {
            playerJson = AddNewPlayerInventory(String.valueOf(playerId));
        }

        try {
            Map<String, Integer> playerData = objectMapper.readValue(playerJson, Map.class);
            int incubatorInUse = playerData.getOrDefault("incubatorInUse", 0);

            if (incubatorInUse > 0) {
                playerData.put("incubatorInUse", incubatorInUse - 1);
                playerJson = objectMapper.writeValueAsString(playerData);
                redisTemplate.opsForValue().set(String.valueOf(playerId), playerJson);
            }
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean useIncubator(int playerId) {
        ObjectMapper objectMapper = new ObjectMapper();


        String playerJson = redisTemplate.opsForValue().get(String.valueOf(playerId));

        if (playerJson == null) {
            playerJson = AddNewPlayerInventory(String.valueOf(playerId));
        }

        try {
            Map<String, Integer> playerData = objectMapper.readValue(playerJson, Map.class);
            int incubatorInUse = playerData.getOrDefault("incubatorInUse", 0);
            int incubatorQuantity = playerData.getOrDefault("incubatorQuantity", 0);
            int eggQuantity = playerData.getOrDefault("eggQuantity", 0);

            if (incubatorInUse < incubatorQuantity && eggQuantity > 0 && incubatorQuantity > 0) {
                playerData.put("incubatorInUse", incubatorInUse + 1);
                playerData.put("eggQuantity", eggQuantity - 1);
                playerJson = objectMapper.writeValueAsString(playerData);
                redisTemplate.opsForValue().set(String.valueOf(playerId), playerJson);

                return true;
            }
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    public void addObjectToInventory(String name, int playerId) {
        ObjectMapper objectMapper = new ObjectMapper();
        String playerJson = redisTemplate.opsForValue().get(String.valueOf(playerId));

        if (playerJson == null) {
            playerJson = AddNewPlayerInventory(String.valueOf(playerId));
        }

        try {
            Map<String, Integer> playerData = objectMapper.readValue(playerJson, Map.class);
            if (name.equals("Egg")) {
                int eggQuantity = playerData.getOrDefault("eggQuantity", 0);
                playerData.put("eggQuantity", eggQuantity + 1);
            } else {
                int incubatorQuantity = playerData.getOrDefault("incubatorQuantity", 0);
                playerData.put("incubatorQuantity", incubatorQuantity + 1);
            }

            playerJson = objectMapper.writeValueAsString(playerData);
            redisTemplate.opsForValue().set(String.valueOf(playerId), playerJson);
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }
    }

    public boolean removeEggToInventory(int playerId) {
        ObjectMapper objectMapper = new ObjectMapper();

        String playerJson = redisTemplate.opsForValue().get(String.valueOf(playerId));

        if (playerJson == null) {
            playerJson = AddNewPlayerInventory(String.valueOf(playerId));
        }

        try {
            Map<String, Integer> playerData = objectMapper.readValue(playerJson, Map.class);
            int eggQuantity = playerData.getOrDefault("eggQuantity", 0);

            if (eggQuantity > 0) {
                playerData.put("eggQuantity", eggQuantity - 1);
                playerJson = objectMapper.writeValueAsString(playerData);
                redisTemplate.opsForValue().set(String.valueOf(playerId), playerJson);
                return true;
            }
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    public boolean updatePlayerBalance(int number, int playerId) {
        ObjectMapper objectMapper = new ObjectMapper();

        String playerJson = redisTemplate.opsForValue().get(String.valueOf(playerId));

        if (playerJson == null) {
            playerJson = AddNewPlayerInventory(String.valueOf(playerId));
        }

        try {
            Map<String, Integer> playerData = objectMapper.readValue(playerJson, Map.class);
            int playerBalance = playerData.getOrDefault("balance", 0);

            if (playerBalance + number > 0) {
                playerData.put("balance", playerBalance + number);
                playerJson = objectMapper.writeValueAsString(playerData);
                redisTemplate.opsForValue().set(String.valueOf(playerId), playerJson);
                return true;
            } else {
                return false;
            }
        } catch (JsonProcessingException e) {
            System.out.println(e.getMessage());
        }

        return false;
    }

    public String getInventory(int playerId) {
        String playerJson = redisTemplate.opsForValue().get(String.valueOf(playerId));

        if (playerJson == null) {
            return AddNewPlayerInventory(String.valueOf(playerId));
        } else {
            return playerJson;
        }
    }
}

